using Microsoft.AspNetCore.Mvc;
using Macro.Common;
using WeConseil.TypeCritere.Service.Dtos;
using WeConseil.TypeCritere.Service.Entities;
using MassTransit;
using WeConseil.TypeCritere.Contracts;


namespace WeConseil.TypeCritere.Service.Controllers
{
    //https://localhost:7001/swagger/index.html
    [ApiController]
    [Route("typeCriters")]
    public class TypeCriteresController : ControllerBase
    {
        private readonly IRepository<TypeCritereItem> typeCritereRepository;
        private readonly IPublishEndpoint publishEndPoint;
        public TypeCriteresController(IRepository<TypeCritereItem> typeCritereRepository,IPublishEndpoint publishEndPoint)
        {
            this.typeCritereRepository = typeCritereRepository;
            this.publishEndPoint =  publishEndPoint;
        }
        //GET /typeCriters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TypeCritereItemDtos>>> GetAsync()
        {
            var items = (await typeCritereRepository.GetAllAsync()).Select(item => item.AsDto());
            return Ok(items);
        }
        //GET /typeCriters/123456
        [HttpGet("{id}")]
        public async Task<ActionResult<TypeCritereItemDtos>> GetByIdAsync(Guid id)
        {
            var item = await typeCritereRepository.GetAsync(id);
            if (item == null)
            {
                return NotFound();
            }
            return item.AsDto();
        }
        //POST /typeCriters
        [HttpPost]
        public async Task<ActionResult<TypeCritereItemDtos>> PostAsync(CreateTypeCritereItemDto createTypeCritereItemDto)
        {
            var itemtypeCritere = new TypeCritereItem
            {
                Designation = createTypeCritereItemDto.Designation,
                CreateDate = DateTimeOffset.UtcNow,
                IsDeleted = false

            };
            await typeCritereRepository.CreateAsync(itemtypeCritere);
            await publishEndPoint.Publish(new TypeCritereItemCreated(itemtypeCritere.Id,itemtypeCritere.Designation));
            return CreatedAtAction(nameof(GetByIdAsync), new { id = itemtypeCritere.Id }, itemtypeCritere);
        }
        //Update /typeCriters
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(Guid id, UpdateTTypeCritereItemDtos updateTTypeCritereItemDtos)
        {
            var existingTypeCritereItem = await typeCritereRepository.GetAsync(id);
            if (existingTypeCritereItem == null)
            {
                return NotFound();
            }
            existingTypeCritereItem.Designation = updateTTypeCritereItemDtos.Designation;
            await publishEndPoint.Publish(new TypeCritereItemUpdated(existingTypeCritereItem.Id,existingTypeCritereItem.Designation));
            await typeCritereRepository.UpdateAsync(existingTypeCritereItem);
            return NoContent();
        }
        //Update /typeCriters
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var existingTypeCritereItem = await typeCritereRepository.GetAsync(id);
            if (existingTypeCritereItem == null)
            {
                return NotFound();
            }
            existingTypeCritereItem.IsDeleted = true;
            await publishEndPoint.Publish(new TypeCritereItemDeleted(existingTypeCritereItem.Id));
            await typeCritereRepository.UpdateAsync(existingTypeCritereItem);
            return NoContent();
        }
    }
}