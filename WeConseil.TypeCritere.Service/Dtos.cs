using System.ComponentModel.DataAnnotations;

namespace WeConseil.TypeCritere.Service.Dtos
{
    public record TypeCritereItemDtos(Guid Id, string Designation);
    public record CreateTypeCritereItemDto([Required][MinLength(3)][MaxLength(60)] string Designation);
    public record UpdateTTypeCritereItemDtos([Required][MinLength(3)][MaxLength(60)] string Designation);
}