using WeConseil.TypeCritere.Service.Dtos;
using WeConseil.TypeCritere.Service.Entities;

namespace WeConseil.TypeCritere.Service
{
    public static class Extensions
    {
        public static TypeCritereItemDtos AsDto (this TypeCritereItem typeCritereItemItem)
        {
             return new TypeCritereItemDtos(typeCritereItemItem.Id,typeCritereItemItem.Designation);
        }
    }
}