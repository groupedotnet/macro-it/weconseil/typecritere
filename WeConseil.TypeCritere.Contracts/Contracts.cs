namespace WeConseil.TypeCritere.Contracts
{
    public record TypeCritereItemCreated(Guid TypeCritereId, string Designation);
    public record TypeCritereItemUpdated(Guid TypeCritereId, string Designation);
    public record TypeCritereItemDeleted(Guid TypeCritereId);
}